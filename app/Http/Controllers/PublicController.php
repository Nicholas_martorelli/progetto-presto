<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('home', 'index','search','locale');
    }
   
    public function home() {
        // $category = Category::find($category_id);
        $articles = Article::all();
        $articles = Article::where('is_accepted' , true)->orderBy('created_at', 'desc')->take(5)->get();
        return view('welcome',compact('articles'));
        
    }
    public function index($name, $category_id){
        // // dd($cat);
        // $articles = Article::where('category_id', $cat)->get();
        // dd($cat);
        // $category = Category::find($cat);

        // return view('category.articles', compact('articles', 'category'));
        // come parametro formale $name, $category_id
        $category = Category::find($category_id);
        $articles = $category->articles()->where('is_accepted' , true)->orderBy('created_at', 'desc')->paginate(5);
        return view('category.articles', compact('category', 'articles'));
    }
    public function category($category){
        
     $category = Category::find($category);
     $articles = Article::where('category_id', $category)->get();
     $articles = $category->articles()->orderBy('created_at', 'desc')->paginate(10);
     return view('category.articles', compact('category', 'articles'));
    }

    public function search(Request $request) {
        $q = $request->input('q');
        $articles = Article::search($q)-> query(function ($builder) { $builder->with(['category']); })->get();
        return view('search_results', compact('q', 'articles'));
    } 
    
    public function contacts(){
        return view('contact.form');
    }
    public function submit(Request $req){
        $email = $req->input ('email');
        $user = $req->input('user');
        $message = $req->input('message');
        $phone = $req->input('phone');
        $contact = compact('user','message');
        Mail::to($email)->send(new ContactMail($contact));
        return redirect(route('home'))->with('email', 'la tua richiesta è stata inoltrata');
    }

    public function locale($locale)
    {
    session()->put('locale',$locale);
    return redirect()->back();
    }
}