<x-layout>
    <x-navbar>

        @if ($article)
            <div class="container-fluid mt-3 pt-5 carrot-bg">

                <div class="row text-center">
                    <div class="col-12 tc-accent2 fs-5 fw-bold my-3">
                        {{ __('ui.annuncio nr') }} {{ $article->id }}
                    </div>
                </div>
                <div class="row justify-content-center mb-5">
                    <div class="col-12 col-md-8 d-flex mx-1 px-2 border-cstm shadow  bg-base">
                        <div class="row justify-content-center mb-1">
                        <div class="col-12 col-md-5 text-center my-5 ">
                            <p class="border-bottom rounded-pill border-2">
                                <span class="fw-bold  tc-accent2">Utente id:</span> {{ $article->user->id }},
                                <span class="fw-bold  tc-accent2"> Nome:</span> {{ $article->user->name }},
                                <span class="fw-bold  tc-accent2"> Email:</span> {{ $article->user->email }}
                            <p>

                            <h6 class="tc-accent">Titolo Annuncio:</h6>
                            <p class="border-bottom rounded-pill border-2">{{ $article->name }}</p>
                            <h6 class="tc-accent">Descrizione Annuncio:</h6>
                            <p class="border-bottom rounded-pill border-2">{{ $article->description }} Lorem ipsum
                                dolor
                                sit
                                amet consectetur adipisicing elit. Recusandae quisquam consequatur officiis vero
                                laudantium
                                est libero expedita voluptas dolorum at debitis maiores itaque accusantium veritatis
                                impedit
                                obcaecati corrupti, fuga eos.</p>
                            <h6 class="tc-accent">Prezzo:</h6>
                            <p class="border-bottom rounded-pill border-2">{{ $article->price }} $</p>
                            
                        </div>
                        <div class="col-12 col-md-7 my-5 p-2 ps-md-5 border-start">
                            <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                                <div class="carousel-inner">
                                    @foreach ($article->images as $image)
                                        <div class="text-center carousel-item @if ($loop->first) active @endif">
                                            <img class="img-fluid card-img-top" src="{{ $image->getUrl(300, 150) }}"
                                                alt="">
                                            <div class="card-body d-flex justify-content-around p-0">
                                                <div class="col-12 col-md-6 text-start py-2 flex-wrap ">
                                                    <div class="d-flex justify-content-between align-items-center border-bottom rounded-pill border-2">
                                                        <h6 class="tc-accent">Adult:</h6>
                                                        @if ($image->adult == 'VERY_UNLIKELY' || $image->adult == 'UNLIKELY')
                                                            <span class="green"></span>
                                                        @elseif ($image->adult == 'LIKELY' || $image->adult == 'VERY_LIKELY')
                                                            <span class="red"></span>
                                                        @elseif ($image->adult == 'POSSIBLE' )
                                                            <span class="yellow"></span>
                                                        @endif
                                                        {{-- <h6 class=""> {{ $image->adult }}</h6> --}}
                                                    </div>
                                                    <div class="d-flex justify-content-between align-items-center border-bottom rounded-pill border-2">
                                                        <h6 class="tc-accent"> Spoof:</h6>
                                                        @if ($image->spoof == 'VERY_UNLIKELY' || $image->spoof == 'UNLIKELY')
                                                            <span class="green"></span>
                                                        @elseif ($image->spoof == 'LIKELY' || $image->spoof == 'VERY_LIKELY')
                                                            <span class="red"></span>
                                                        @elseif ($image->spoof == 'POSSIBLE' )
                                                            <span class="yellow"></span>
                                                        @endif
                                                    </div>
                                                    <div class="d-flex justify-content-between align-items-center border-bottom rounded-pill border-2">
                                                        <h6 class="tc-accent">Medical:</h6>
                                                        @if ($image->medical == 'VERY_UNLIKELY' || $image->medical == 'UNLIKELY')
                                                            <span class="green"></span>
                                                        @elseif ($image->medical == 'LIKELY' || $image->medical == 'VERY_LIKELY')                                                            
                                                            <span class="red"></span>
                                                        @elseif ($image->medical == 'POSSIBLE' )
                                                            <span class="yellow"></span>
                                                        @endif
                                                    </div>
                                                    <div class="d-flex justify-content-between align-items-center border-bottom rounded-pill border-2">
                                                        <h6 class="tc-accent">Violence:</h6>
                                                        @if ($image->violence == 'VERY_UNLIKELY' || $image->violence == 'UNLIKELY')
                                                            <span class="green"></span>
                                                        @elseif ($image->violence == 'LIKELY' || $image->violence == 'VERY_LIKELY')
                                                            <span class="red"></span>
                                                        @elseif ($image->violence == 'POSSIBLE' )
                                                            <span class="yellow"></span>
                                                        @endif
                                                    </div>
                                                    <div class="d-flex justify-content-between align-items-center border-bottom rounded-pill border-2">
                                                        <h6 class="tc-accent">Racy:</h6>
                                                        @if ($image->racy == 'VERY_UNLIKELY' || $image->racy == 'UNLIKELY')
                                                            <span class="green"></span>
                                                        @elseif ($image->racy == 'LIKELY' || $image->racy == 'VERY_LIKELY')
                                                            <span class="red"></span>
                                                        @elseif ($image->racy == 'POSSIBLE' )
                                                            <span class="yellow"></span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-12 col-md-4 text-end py-2">
                                                    <h6>Etichette</h6>
                                                    <ul class="list-unstyled ">
                                                        @if ($image->labels)
                                                            @foreach ($image->labels as $label)
                                                                <span class="tc-accent">{{ $label }}, </span>
                                                            @endforeach
                                                        @endif
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <button class="carousel-control-prev" type="button"
                                    data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                    <span class="fas fa-arrow-left" aria-hidden="true"></span>
                                    <span class="visually-hidden">Previous</span>
                                </button>
                                <button class="carousel-control-next" type="button"
                                    data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                    <span class="fas fa-arrow-right" aria-hidden="true"></span>
                                    <span class="visually-hidden">Next</span>
                                </button>
                            </div>
                        </div>
                        <div class="row justify-content-between d-wrap sparisce-sm border-top ">
                            <div class=" col-12 col-md-6 text-center mt-2 mb-2 px-5">
                                <form action="{{ route('revisor.reject', $article->id) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-custom4">Reject</button>
                                </form>
                            </div>
                            <div class="col-12 col-md-6 mt-2 mb-5 px-5 text-center">
                                <form action="{{ route('revisor.accept', $article->id) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-custom5">Accept</button>
                                </form>
                            </div>
                        </div>
                        <div class="row justify-content-center appare-sm">
                            <div class=" col-6 mt-5 mb-5 px-5">
                                <form action="{{ route('revisor.reject', $article->id) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-custom4">Reject</button>
                                </form>
                            </div>
                            <div class="col-6 mt-5 mb-5 px-5">
                                <form action="{{ route('revisor.accept', $article->id) }}" method="POST">
                                    @csrf
                                    <button type="submit" class="btn btn-custom5">Accept</button>
                                </form>
                            </div>
                        </div>

                        </div>
                    </div>
                @else
                    <div class="my-5 p-5 text-center">
                        <h1>{{ __('ui.titolo') }}</h1>
                        <h3>{{ __('ui.sotto-titolo') }}</h3>
                    </div>
        @endif

        </div>






    </x-navbar>
</x-layout>
