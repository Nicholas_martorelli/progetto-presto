<x-layout>
    <x-navbar>



        <div class="container my-5 p-5">
            <div class="row">
                <div class="col-12 ">
                    <h1 class="text-center">{{ $article->name }}</h1>
                </div>
                <div class="row ">
                    <div class="col-12 col-md-6 my-5">
                        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-inner">
                                @foreach ($article->images as $image)
                                <div class="text-center carousel-item @if ($loop->first) active @endif">
                                        <img class="img-fluid card-img-top" src="{{ $image->getUrl(300, 150) }}"
                                            alt="">
                                </div>
                                @endforeach
                            </div>
                            <button class="carousel-control-prev" type="button"
                                data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                                <span class="fas fa-arrow-left" aria-hidden="true"></span>
                                <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control-next" type="button"
                                data-bs-target="#carouselExampleControls" data-bs-slide="next">
                                <span class="fas fa-arrow-right" aria-hidden="true"></span>
                                <span class="visually-hidden">Next</span>
                            </button>
                        </div>
                        {{-- @if ($article->img)
                        <img src="{{ Storage::url($article->img) }}"class="card-img-top your-class img-fluid" alt="">
                        @else
                        <img src="/img/200x100.png" class="card-img-top img-fluid" alt="">
                        @endif --}}
                    </div>
                    <div class="col-12 col-md-4 offset-md-2 my-5 word-wrap">
                        <div class="col-12">
                            <p>{{ $article->description }} </p>
                            <h5 class=" fw-bold">{{ $article->price }} $</h5>
                            <h5 class=" fw-bold">{{__('ui.contattami qui')}} {{ $article->contact }} </h5>
                            <h5 class="card-text fw-bold"> {{ $article->category->name }} </h5>
                            <p>{{__('ui.creato il')}} {{ $article->created_at->format('d/m/y') }} {{__('ui.da')}} {{ $article->user->name }}
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row text-center">
                    <div class="col-12">
                        <a href="{{ route('home') }}" class="btn btn-primary btn-custom">{{__('ui.torna alla home')}}</a>
                    </div>
                </div>
            </div>
        </div>




    </x-navbar>
</x-layout>


{{-- <div class="card card-custom my-5 mx-5">
    <div class="card-body">
        <img src="{{ Storage::url($article->img) }}" class="card-img-top img-fluid rounded-circle"
            alt="">
        <h4 class="card-text fw-bold"></h4>
        <h5 class="card-text fw-bold">{{ $article->description }} </h5>
        <h5 class="card-text fw-bold">{{ $article->price }} $</h5>
        <a href="{{ route('home') }}" class="btn btn-primary btn-custom">Torna alla Home</a>
    </div>
    <i class="fas fa-carrot card-carrot "></i>
</div> --}}
