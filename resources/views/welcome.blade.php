<x-layout>
    <x-navbar>
        {{-- HEADER --}}
        {{-- messaggio --}}
        <div class="container-fluid">
            @if (session('message'))
                <div class="mt-5 pt-5">
                    <div class="rounded-circle alert fs-3 tc-base text-center alert-danger mt-5 pt-5">
                        <p class="text-middle">{{ ucfirst(session('message')) }}</p> 
                    </div>
                </div>
            @endif
            {{-- messaggio fine --}}
            {{-- 'messaggio mail' --}}
            @if (session('email'))
                <div class="mt-5 pt-1 ">
                    <div class="rounded-circle alert fs-3 tc-base text-center alert-danger mt-5 pt-5">
                        <p class="text-middle">{{ ucfirst(session('email')) }}</p>                      
                    </div>
                </div>
            @endif
            {{-- 'messaggio mail fine --}}

            {{-- messaggio revisor --}}
            @if (session('article.created.success'))
                <div class="rounded-circle alert fs-3 tc-main text-center bg-base mt-5 pt-5">
                    {{ __('ui.creato con succ') }}
                </div>
            @endif
            @if (session('access.denied.revisor.only'))
                <div class="rounded-circle alert fs-3 tc-main text-center bg-base mt-5 pt-5">
                    {{ __('ui.solo rev') }}
                </div>
            @endif
            {{-- messaggio fine --}}
            <header class="masthead">
                <div class="container h-100">
                    <div class="row h-100 align-items-end align-items-md-center arrow-align-custom">
                        <div class="col-12 text-left justify-content-center">
                            <div class="col-12 col-lg-5 border-home text-center text-lg-start w-sm-100 w-md-50 w-lg-25">
                                <h1 class="font-weight-light fw-bold text-center">{{ __('ui.welcome') }} </h1>
                                <p class="lead fs-2 text-center">{{ __('ui.segui') }}</p>
                            </div>
                            <div class="row justify-content-center justify-content-lg-start">
                                <div class="col-3 d-flex justify-content-center justify-content-lg-end">
                                    <button class="btn-arrow text-center">
                                        <a href="#down" alt="">
                                            <i class="fas fa-location-arrow"></i>
                                        </a>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </div>
        {{-- inizio card categorie --}}
        <div class="container pt-5 mt-5" id="down">
            <div class="row pt-3">
                <div class="col-12">
                    <h2 class="tc-accent text-center category-border"> <span class="tc-sec ">
                            {{ __('ui.Le nostre') }}</span> {{ __('ui.categorie') }}
                    </h2>
                    <div class="pronto-carousel text-center">


                        <div class="container my-5">
                            <div class="row my-2 " id="">
                                <div class="col-12 slider-for">
                                    @foreach ($categories as $category)
                                        <div>
                                            <div class="card card-custom my-5 mx-1">
                                                <div class="card-body">
                                                    {{-- ${categoria.icon} --}}
                                                    <h4 class="card-text fw-bold">{{ ucfirst($category->name) }}</h4>
                                                    <a href="{{ route('article.category', ['category' => $category->id]) }}"
                                                        class="btn btn-primary btn-custom">{{ __('ui.vai') }}</a>
                                                </div>
                                                <i class="fas fa-carrot card-carrot " style="font-size: 170px"></i>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- inizio spazio per gli ultimi 5 annunci --}}
        <div class="container pt-5">
            <div class="row">
                <div class="col-12">
                    <h2 class="tc-accent text-center category-border"> <span class="tc-sec "> {{ __('ui.ultimi') }}
                        </span> {{ __('ui.annunci') }}
                    </h2>
                    <div class="pronto-carousel text-center">


                        <div class="container my-5">
                            <div class="row my-2" id="down">
                                <div class="col-12 slider-for">
                                    @foreach ($articles as $article)
                                        <div>
                                            <div class="card card-custom-carousel my-5 mx-5 px-4">

                                                <div class="card-body ">
                                                    @foreach ($article->images as $image)
                                                        @if ($loop->first)
                                                            <img class="img-fluid "
                                                                src="{{ $image->getUrl(300, 150) }}" alt="">
                                                        @endif

                                                        {{ $article->body }}


                                                    @endforeach

                                                    <h4 class="card-text fw-bold">{{ $article->name }}</h4>
                                                    <h5 class="card-text fw-bold text-truncate ">
                                                        {{ $article->description }}</h5>
                                                    <h5 class="card-text fw-bold">{{ $article->price }} $</h5>
                                                    <a
                                                        href="{{ route('category.articles', [$article->category->name, $article->category->id]) }}">
                                                        {{ $article->category->name }}</a>

                                                    <p>{{ __('ui.creato il') }}
                                                        {{ $article->created_at->format('d/m/y') }}
                                                        {{ __('ui.da') }}
                                                        {{ $article->user->name }}</p>
                                                    <a href="{{ route('article.show', compact('article')) }}"
                                                        class="btn btn-primary btn-custom">{{ __('ui.dettaglio') }}</a>
                                                </div>
                                                <i class="fas fa-carrot card-carrot2 "></i>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- container per l'email --}}
        <div class="container-fluid my-5">
            <div class="box-3 text-center img-fluid">
                <div class="row">
                    <div class="col-12">
                        <h3 class="tc-accent my-5">{{ __('ui.entra nella tana') }}</h3>
                        <a href="{{ route('contact.form') }}"
                            class="btn btn-primary btn-custom">{{ __('ui.bottone email') }}</a><i
                            class="icofont-rabbit icon-logo"></i>
                    </div>
                </div>
            </div>
        </div>


    </x-navbar>
    @push('lowscript')
        <script>
            $('.slider-for').slick({
                centerMode: true,
                centerPadding: '60px',
                slidesToShow: 3,
                cssEase: 'linear',
                variableWidth: true,
                variableHeight: true,
                responsive: [{

                        breakpoint: 768,
                        settings: {
                            arrows: true,
                            centerMode: true,
                            centerPadding: '40px',
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            arrows: false,
                            centerMode: true,
                            centerPadding: '0px',
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });

        </script>
    @endpush
</x-layout>
